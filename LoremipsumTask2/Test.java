package Tests;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;

import java.util.List;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;

public class Test {
  private WebDriver driver;

  @BeforeTest
  public void profileSetUp(){
    chromedriver().setup();
  }

  @BeforeMethod
  public void testsSetUp() {
    driver = new ChromeDriver();
    driver.manage().window().maximize();
    driver.get("https://www.lipsum.com/");
  }

@org.testng.annotations.Test
  public void checkTheWord(){

    WebElement russianLanguageOfSite = driver.findElement(By.xpath("//a[@href='http://ru.lipsum.com/']"));
    russianLanguageOfSite.click();
    WebElement element = driver.findElement(By.xpath("//*[@id=\"Panes\"]/div[1]/p"));
    String elementText = element.getText();
	Assert.assertTrue(elementText.contains("рыба")); 
}
@org.testng.annotations.Test
public void checkThatDefaultSettingResultInTextStartingWithLoremIpsum(){
    WebElement generateLI=driver.findElement(By.xpath("//input[@name='generate']"));
    generateLI.click();
   new WebDriverWait(driver, 30).until(
          webDriver -> ((JavascriptExecutor) webDriver).executeScript("return document.readyState").equals("complete"));
  WebElement firstParagraph=driver.findElement(By.xpath("//*[@id=\"lipsum\"]/p[1]"));

  String textOfParagraph = firstParagraph.getText();
  boolean isFirstParagraphStartWith=textOfParagraph.startsWith("Lorem ipsum dolor sit amet, consectetur adipiscing elit");
  Assert.assertTrue(isFirstParagraphStartWith);
}

@org.testng.annotations.Test
public  void verifyThatLoremIpsumIsGeneratedWithCorrectSize(){
    WebElement inputWord=driver.findElement(By.xpath("//input[@value='words']"));
    inputWord.click();
    WebElement inputAmount=driver.findElement(By.xpath("//input[@name='amount']"));
    inputAmount.clear();
    inputAmount.sendKeys("10");
  WebElement generateLI=driver.findElement(By.xpath("//input[@name='generate']"));
  generateLI.click();
  WebElement firstParagraph=driver.findElement(By.xpath("//*[@id='lipsum']/p"));

  String textOfParagraph = firstParagraph.getText();
  int blockCount = textOfParagraph.split(" ").length;
  Assert.assertEquals(blockCount,10);
  }
  @org.testng.annotations.Test
  public  void verifyThatLoremIpsumIsGeneratedWithCorrectSize_5() {
    WebElement inputWord = driver.findElement(By.xpath("//input[@value='words']"));
    inputWord.click();
    WebElement inputAmount = driver.findElement(By.xpath("//input[@name='amount']"));
    inputAmount.clear();
    inputAmount.sendKeys("5");
    WebElement generateLI = driver.findElement(By.xpath("//input[@name='generate']"));
    generateLI.click();
    WebElement firstParagraph = driver.findElement(By.xpath("//*[@id='lipsum']/p"));

    String textOfParagraph = firstParagraph.getText();
    int blockCount = textOfParagraph.split(" ").length;
    Assert.assertEquals(blockCount, 5);
  }
  @org.testng.annotations.Test
  public  void verifyThatLoremIpsumIsGeneratedWithCorrectSize_0() {
    WebElement inputWord = driver.findElement(By.xpath("//input[@value='words']"));
    inputWord.click();
    WebElement inputAmount = driver.findElement(By.xpath("//input[@name='amount']"));
    inputAmount.clear();
    inputAmount.sendKeys("1");
    WebElement generateLI = driver.findElement(By.xpath("//input[@name='generate']"));
    generateLI.click();
    WebElement firstParagraph = driver.findElement(By.xpath("//*[@id='lipsum']/p"));

    String textOfParagraph = firstParagraph.getText();
    int blockCount = textOfParagraph.split(" ").length;
    Assert.assertEquals(blockCount, 1);
  }
@org.testng.annotations.Test
  public  void verifyThatLoremIpsumIsGeneratedWithCorrectSizeByte_0() {
    WebElement inputWord = driver.findElement(By.xpath("//input[@value='bytes']"));
    inputWord.click();
    WebElement inputBytes = driver.findElement(By.xpath("//input[@name='amount']"));
    inputBytes.clear();
    inputBytes.sendKeys("10");
    WebElement generateLI = driver.findElement(By.xpath("//input[@name='generate']"));
    generateLI.click();
    WebElement firstParagraph = driver.findElement(By.xpath("//*[@id='lipsum']/p"));

    String textOfParagraph = firstParagraph.getText();
    byte[] byteArray = textOfParagraph.getBytes();
    int byteCount = byteArray.length;
    Assert.assertEquals(byteCount, 10);
  }

  @org.testng.annotations.Test
  public void UncheckStartWithLoremIpsumCheckbox(){
    WebElement checkboxStartWith = driver.findElement(By.xpath("//input[@type='checkbox']"));
    checkboxStartWith.click();
    WebElement generateLI = driver.findElement(By.xpath("//input[@name='generate']"));
    generateLI.click();
    WebElement firstParagraph = driver.findElement(By.xpath("//*[@id='lipsum']/p"));

    String textOfParagraph = firstParagraph.getText();
    Assert.assertFalse(textOfParagraph.startsWith("Lorem ipsum dolor sit amet, consectetur adipiscing elit"));
  }
  @org.testng.annotations.Test
  public void checkQuantityWordsLorem(){
      int qLorem=0;
      for (int i = 0; i < 11; i++) {
        driver.get("https://www.lipsum.com/");
        WebElement generateLI = driver.findElement(By.xpath("//input[@name='generate']"));
        generateLI.click();
        List<WebElement> listOfParagraph = driver.findElements(By.xpath("//div//p"));

        int quantityLorem = 0;
        for (WebElement webElement : listOfParagraph) {
          if (webElement.getText().contains("lorem"))
            quantityLorem++;
        }
        qLorem = qLorem+quantityLorem;
      }
    double average = 0;
    average=qLorem/11;
    boolean containsLoremMoreThan40Percent=false;
    if (average>=2) containsLoremMoreThan40Percent=true;
    Assert.assertTrue(containsLoremMoreThan40Percent);
    }





  @AfterMethod
  public void tearDown () {
    driver.quit();
  }
}
